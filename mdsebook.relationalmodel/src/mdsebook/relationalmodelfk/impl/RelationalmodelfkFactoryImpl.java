/**
 */
package mdsebook.relationalmodelfk.impl;

import mdsebook.relationalmodelfk.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class RelationalmodelfkFactoryImpl extends EFactoryImpl implements RelationalmodelfkFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static RelationalmodelfkFactory init() {
		try {
			RelationalmodelfkFactory theRelationalmodelfkFactory = (RelationalmodelfkFactory)EPackage.Registry.INSTANCE.getEFactory(RelationalmodelfkPackage.eNS_URI);
			if (theRelationalmodelfkFactory != null) {
				return theRelationalmodelfkFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new RelationalmodelfkFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RelationalmodelfkFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case RelationalmodelfkPackage.INTEGER_COLUMN: return createIntegerColumn();
			case RelationalmodelfkPackage.TABLE: return createTable();
			case RelationalmodelfkPackage.ROOT: return createRoot();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IntegerColumn createIntegerColumn() {
		IntegerColumnImpl integerColumn = new IntegerColumnImpl();
		return integerColumn;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Table createTable() {
		TableImpl table = new TableImpl();
		return table;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Root createRoot() {
		RootImpl root = new RootImpl();
		return root;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RelationalmodelfkPackage getRelationalmodelfkPackage() {
		return (RelationalmodelfkPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static RelationalmodelfkPackage getPackage() {
		return RelationalmodelfkPackage.eINSTANCE;
	}

} //RelationalmodelfkFactoryImpl
