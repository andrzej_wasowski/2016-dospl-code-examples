/**
 */
package mdsebook.relationalmodelfk;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Integer Column</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * Assume that all columns are of integer type for simplicity, so no need to have a 'type' property.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link mdsebook.relationalmodelfk.IntegerColumn#getRefersTo <em>Refers To</em>}</li>
 * </ul>
 *
 * @see mdsebook.relationalmodelfk.RelationalmodelfkPackage#getIntegerColumn()
 * @model
 * @generated
 */
public interface IntegerColumn extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Refers To</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Refers To</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Refers To</em>' reference.
	 * @see #setRefersTo(Table)
	 * @see mdsebook.relationalmodelfk.RelationalmodelfkPackage#getIntegerColumn_RefersTo()
	 * @model ordered="false"
	 * @generated
	 */
	Table getRefersTo();

	/**
	 * Sets the value of the '{@link mdsebook.relationalmodelfk.IntegerColumn#getRefersTo <em>Refers To</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Refers To</em>' reference.
	 * @see #getRefersTo()
	 * @generated
	 */
	void setRefersTo(Table value);

} // IntegerColumn
