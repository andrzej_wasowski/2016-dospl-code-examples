/**
 */
package mdsebook.featuremodels1;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Xor Group1</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see mdsebook.featuremodels1.Featuremodels1Package#getXorGroup1()
 * @model
 * @generated
 */
public interface XorGroup1 extends Group1 {
} // XorGroup1
