/**
 */
package mdsebook.featuremodels1.impl;

import mdsebook.featuremodels1.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class Featuremodels1FactoryImpl extends EFactoryImpl implements Featuremodels1Factory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static Featuremodels1Factory init() {
		try {
			Featuremodels1Factory theFeaturemodels1Factory = (Featuremodels1Factory)EPackage.Registry.INSTANCE.getEFactory(Featuremodels1Package.eNS_URI);
			if (theFeaturemodels1Factory != null) {
				return theFeaturemodels1Factory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new Featuremodels1FactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Featuremodels1FactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case Featuremodels1Package.MODEL1: return createModel1();
			case Featuremodels1Package.FEATURE1: return createFeature1();
			case Featuremodels1Package.OR_GROUP1: return createOrGroup1();
			case Featuremodels1Package.XOR_GROUP1: return createXorGroup1();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Model1 createModel1() {
		Model1Impl model1 = new Model1Impl();
		return model1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Feature1 createFeature1() {
		Feature1Impl feature1 = new Feature1Impl();
		return feature1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OrGroup1 createOrGroup1() {
		OrGroup1Impl orGroup1 = new OrGroup1Impl();
		return orGroup1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XorGroup1 createXorGroup1() {
		XorGroup1Impl xorGroup1 = new XorGroup1Impl();
		return xorGroup1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Featuremodels1Package getFeaturemodels1Package() {
		return (Featuremodels1Package)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static Featuremodels1Package getPackage() {
		return Featuremodels1Package.eINSTANCE;
	}

} //Featuremodels1FactoryImpl
