/**
 */
package mdsebook.featuremodels1.impl;

import mdsebook.featuremodels1.Featuremodels1Package;
import mdsebook.featuremodels1.XorGroup1;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Xor Group1</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class XorGroup1Impl extends Group1Impl implements XorGroup1 {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected XorGroup1Impl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Featuremodels1Package.Literals.XOR_GROUP1;
	}

} //XorGroup1Impl
