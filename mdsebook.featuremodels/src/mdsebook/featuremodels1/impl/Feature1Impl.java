/**
 */
package mdsebook.featuremodels1.impl;

import java.util.Collection;

import mdsebook.featuremodels1.Feature1;
import mdsebook.featuremodels1.Featuremodels1Package;
import mdsebook.featuremodels1.Group1;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Feature1</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link mdsebook.featuremodels1.impl.Feature1Impl#getSubfeatures <em>Subfeatures</em>}</li>
 *   <li>{@link mdsebook.featuremodels1.impl.Feature1Impl#getGroups <em>Groups</em>}</li>
 * </ul>
 *
 * @generated
 */
public class Feature1Impl extends NamedElement1Impl implements Feature1 {
	/**
	 * The cached value of the '{@link #getSubfeatures() <em>Subfeatures</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSubfeatures()
	 * @generated
	 * @ordered
	 */
	protected EList<Feature1> subfeatures;

	/**
	 * The cached value of the '{@link #getGroups() <em>Groups</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGroups()
	 * @generated
	 * @ordered
	 */
	protected EList<Group1> groups;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Feature1Impl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Featuremodels1Package.Literals.FEATURE1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Feature1> getSubfeatures() {
		if (subfeatures == null) {
			subfeatures = new EObjectContainmentEList<Feature1>(Feature1.class, this, Featuremodels1Package.FEATURE1__SUBFEATURES);
		}
		return subfeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Group1> getGroups() {
		if (groups == null) {
			groups = new EObjectContainmentEList<Group1>(Group1.class, this, Featuremodels1Package.FEATURE1__GROUPS);
		}
		return groups;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Featuremodels1Package.FEATURE1__SUBFEATURES:
				return ((InternalEList<?>)getSubfeatures()).basicRemove(otherEnd, msgs);
			case Featuremodels1Package.FEATURE1__GROUPS:
				return ((InternalEList<?>)getGroups()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Featuremodels1Package.FEATURE1__SUBFEATURES:
				return getSubfeatures();
			case Featuremodels1Package.FEATURE1__GROUPS:
				return getGroups();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Featuremodels1Package.FEATURE1__SUBFEATURES:
				getSubfeatures().clear();
				getSubfeatures().addAll((Collection<? extends Feature1>)newValue);
				return;
			case Featuremodels1Package.FEATURE1__GROUPS:
				getGroups().clear();
				getGroups().addAll((Collection<? extends Group1>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Featuremodels1Package.FEATURE1__SUBFEATURES:
				getSubfeatures().clear();
				return;
			case Featuremodels1Package.FEATURE1__GROUPS:
				getGroups().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Featuremodels1Package.FEATURE1__SUBFEATURES:
				return subfeatures != null && !subfeatures.isEmpty();
			case Featuremodels1Package.FEATURE1__GROUPS:
				return groups != null && !groups.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //Feature1Impl
