/**
 */
package mdsebook.sql;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Select Query</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link mdsebook.sql.SelectQuery#getWhat <em>What</em>}</li>
 *   <li>{@link mdsebook.sql.SelectQuery#getFrom <em>From</em>}</li>
 * </ul>
 *
 * @see mdsebook.sql.SqlPackage#getSelectQuery()
 * @model
 * @generated
 */
public interface SelectQuery extends EObject {
	/**
	 * Returns the value of the '<em><b>What</b></em>' reference list.
	 * The list contents are of type {@link mdsebook.sql.Column}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>What</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>What</em>' reference list.
	 * @see mdsebook.sql.SqlPackage#getSelectQuery_What()
	 * @model required="true"
	 * @generated
	 */
	EList<Column> getWhat();

	/**
	 * Returns the value of the '<em><b>From</b></em>' reference list.
	 * The list contents are of type {@link mdsebook.sql.Table}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>From</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>From</em>' reference list.
	 * @see mdsebook.sql.SqlPackage#getSelectQuery_From()
	 * @model required="true"
	 * @generated
	 */
	EList<Table> getFrom();

} // SelectQuery
