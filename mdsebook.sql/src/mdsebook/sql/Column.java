/**
 */
package mdsebook.sql;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Column</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see mdsebook.sql.SqlPackage#getColumn()
 * @model
 * @generated
 */
public interface Column extends NamedElement {
} // Column
