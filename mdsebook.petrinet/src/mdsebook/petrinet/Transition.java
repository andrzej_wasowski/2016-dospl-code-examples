/**
 */
package mdsebook.petrinet;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Transition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link mdsebook.petrinet.Transition#getToPlace <em>To Place</em>}</li>
 *   <li>{@link mdsebook.petrinet.Transition#getNet <em>Net</em>}</li>
 *   <li>{@link mdsebook.petrinet.Transition#getFromPlace <em>From Place</em>}</li>
 *   <li>{@link mdsebook.petrinet.Transition#getInput <em>Input</em>}</li>
 * </ul>
 *
 * @see mdsebook.petrinet.PetrinetPackage#getTransition()
 * @model
 * @generated
 */
public interface Transition extends EObject {
	/**
	 * Returns the value of the '<em><b>To Place</b></em>' reference list.
	 * The list contents are of type {@link mdsebook.petrinet.Place}.
	 * It is bidirectional and its opposite is '{@link mdsebook.petrinet.Place#getIncomingTransitions <em>Incoming Transitions</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>To Place</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>To Place</em>' reference list.
	 * @see mdsebook.petrinet.PetrinetPackage#getTransition_ToPlace()
	 * @see mdsebook.petrinet.Place#getIncomingTransitions
	 * @model opposite="incomingTransitions"
	 * @generated
	 */
	EList<Place> getToPlace();

	/**
	 * Returns the value of the '<em><b>Net</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link mdsebook.petrinet.PetriNet#getTransitions <em>Transitions</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Net</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Net</em>' container reference.
	 * @see #setNet(PetriNet)
	 * @see mdsebook.petrinet.PetrinetPackage#getTransition_Net()
	 * @see mdsebook.petrinet.PetriNet#getTransitions
	 * @model opposite="transitions" required="true" transient="false"
	 * @generated
	 */
	PetriNet getNet();

	/**
	 * Sets the value of the '{@link mdsebook.petrinet.Transition#getNet <em>Net</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Net</em>' container reference.
	 * @see #getNet()
	 * @generated
	 */
	void setNet(PetriNet value);

	/**
	 * Returns the value of the '<em><b>From Place</b></em>' reference list.
	 * The list contents are of type {@link mdsebook.petrinet.Place}.
	 * It is bidirectional and its opposite is '{@link mdsebook.petrinet.Place#getOutgoingTransitions <em>Outgoing Transitions</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>From Place</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>From Place</em>' reference list.
	 * @see mdsebook.petrinet.PetrinetPackage#getTransition_FromPlace()
	 * @see mdsebook.petrinet.Place#getOutgoingTransitions
	 * @model opposite="outgoingTransitions"
	 * @generated
	 */
	EList<Place> getFromPlace();

	/**
	 * Returns the value of the '<em><b>Input</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Input</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Input</em>' attribute.
	 * @see #setInput(String)
	 * @see mdsebook.petrinet.PetrinetPackage#getTransition_Input()
	 * @model
	 * @generated
	 */
	String getInput();

	/**
	 * Sets the value of the '{@link mdsebook.petrinet.Transition#getInput <em>Input</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Input</em>' attribute.
	 * @see #getInput()
	 * @generated
	 */
	void setInput(String value);

} // Transition
