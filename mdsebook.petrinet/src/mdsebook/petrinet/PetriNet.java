/**
 */
package mdsebook.petrinet;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Petri Net</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link mdsebook.petrinet.PetriNet#getPlaces <em>Places</em>}</li>
 *   <li>{@link mdsebook.petrinet.PetriNet#getTransitions <em>Transitions</em>}</li>
 * </ul>
 *
 * @see mdsebook.petrinet.PetrinetPackage#getPetriNet()
 * @model
 * @generated
 */
public interface PetriNet extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Places</b></em>' containment reference list.
	 * The list contents are of type {@link mdsebook.petrinet.Place}.
	 * It is bidirectional and its opposite is '{@link mdsebook.petrinet.Place#getNet <em>Net</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Places</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Places</em>' containment reference list.
	 * @see mdsebook.petrinet.PetrinetPackage#getPetriNet_Places()
	 * @see mdsebook.petrinet.Place#getNet
	 * @model opposite="net" containment="true"
	 * @generated
	 */
	EList<Place> getPlaces();

	/**
	 * Returns the value of the '<em><b>Transitions</b></em>' containment reference list.
	 * The list contents are of type {@link mdsebook.petrinet.Transition}.
	 * It is bidirectional and its opposite is '{@link mdsebook.petrinet.Transition#getNet <em>Net</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Transitions</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Transitions</em>' containment reference list.
	 * @see mdsebook.petrinet.PetrinetPackage#getPetriNet_Transitions()
	 * @see mdsebook.petrinet.Transition#getNet
	 * @model opposite="net" containment="true"
	 * @generated
	 */
	EList<Transition> getTransitions();

} // PetriNet
