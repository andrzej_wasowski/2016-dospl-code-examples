/**
 */
package mdsebook.petrinet;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Place</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link mdsebook.petrinet.Place#getTokenNo <em>Token No</em>}</li>
 *   <li>{@link mdsebook.petrinet.Place#getIncomingTransitions <em>Incoming Transitions</em>}</li>
 *   <li>{@link mdsebook.petrinet.Place#getNet <em>Net</em>}</li>
 *   <li>{@link mdsebook.petrinet.Place#getOutgoingTransitions <em>Outgoing Transitions</em>}</li>
 * </ul>
 *
 * @see mdsebook.petrinet.PetrinetPackage#getPlace()
 * @model
 * @generated
 */
public interface Place extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Token No</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Token No</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Token No</em>' attribute.
	 * @see #setTokenNo(int)
	 * @see mdsebook.petrinet.PetrinetPackage#getPlace_TokenNo()
	 * @model
	 * @generated
	 */
	int getTokenNo();

	/**
	 * Sets the value of the '{@link mdsebook.petrinet.Place#getTokenNo <em>Token No</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Token No</em>' attribute.
	 * @see #getTokenNo()
	 * @generated
	 */
	void setTokenNo(int value);

	/**
	 * Returns the value of the '<em><b>Incoming Transitions</b></em>' reference list.
	 * The list contents are of type {@link mdsebook.petrinet.Transition}.
	 * It is bidirectional and its opposite is '{@link mdsebook.petrinet.Transition#getToPlace <em>To Place</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Incoming Transitions</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Incoming Transitions</em>' reference list.
	 * @see mdsebook.petrinet.PetrinetPackage#getPlace_IncomingTransitions()
	 * @see mdsebook.petrinet.Transition#getToPlace
	 * @model opposite="toPlace"
	 * @generated
	 */
	EList<Transition> getIncomingTransitions();

	/**
	 * Returns the value of the '<em><b>Net</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link mdsebook.petrinet.PetriNet#getPlaces <em>Places</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Net</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Net</em>' container reference.
	 * @see #setNet(PetriNet)
	 * @see mdsebook.petrinet.PetrinetPackage#getPlace_Net()
	 * @see mdsebook.petrinet.PetriNet#getPlaces
	 * @model opposite="places" required="true" transient="false"
	 * @generated
	 */
	PetriNet getNet();

	/**
	 * Sets the value of the '{@link mdsebook.petrinet.Place#getNet <em>Net</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Net</em>' container reference.
	 * @see #getNet()
	 * @generated
	 */
	void setNet(PetriNet value);

	/**
	 * Returns the value of the '<em><b>Outgoing Transitions</b></em>' reference list.
	 * The list contents are of type {@link mdsebook.petrinet.Transition}.
	 * It is bidirectional and its opposite is '{@link mdsebook.petrinet.Transition#getFromPlace <em>From Place</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Outgoing Transitions</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Outgoing Transitions</em>' reference list.
	 * @see mdsebook.petrinet.PetrinetPackage#getPlace_OutgoingTransitions()
	 * @see mdsebook.petrinet.Transition#getFromPlace
	 * @model opposite="fromPlace"
	 * @generated
	 */
	EList<Transition> getOutgoingTransitions();

} // Place
