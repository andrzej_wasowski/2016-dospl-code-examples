// (c) mddbook, wasowski, tberger

lazy val root = (project in file(".")).settings (

  organization := "mdsebook",
  name := "mdsebook.numbers.scala",
  version := "0.03",
  // stick to Scala version supported by Scala IDE 
  scalaVersion := "2.11.8",
  retrieveManaged := true,
  EclipseKeys.relativizeLibs := true,

  scalacOptions += "-deprecation",
  scalacOptions += "-feature",
  scalacOptions += "-language:implicitConversions",

  libraryDependencies += "org.eclipse.emf" % "org.eclipse.emf.ecore" % "2.11.+",
  libraryDependencies += "org.eclipse.emf" % "org.eclipse.emf.ecore.xmi" % "2.11.+",
  libraryDependencies += "org.eclipse.emf" % "org.eclipse.emf.common" % "2.11.+",
  libraryDependencies += "org.bitbucket.inkytonik.kiama" %% "kiama" % "2.0.0",
  libraryDependencies += "org.scalatest" %% "scalatest" % "2.2.6" % "test"

)
