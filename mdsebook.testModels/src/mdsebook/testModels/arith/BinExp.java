/**
 */
package mdsebook.testModels.arith;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Bin Exp</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link mdsebook.testModels.arith.BinExp#getLeft <em>Left</em>}</li>
 *   <li>{@link mdsebook.testModels.arith.BinExp#getRight <em>Right</em>}</li>
 * </ul>
 *
 * @see mdsebook.testModels.arith.ArithPackage#getBinExp()
 * @model abstract="true"
 * @generated
 */
public interface BinExp extends Exp {
	/**
	 * Returns the value of the '<em><b>Left</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Left</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Left</em>' containment reference.
	 * @see #setLeft(Exp)
	 * @see mdsebook.testModels.arith.ArithPackage#getBinExp_Left()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Exp getLeft();

	/**
	 * Sets the value of the '{@link mdsebook.testModels.arith.BinExp#getLeft <em>Left</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Left</em>' containment reference.
	 * @see #getLeft()
	 * @generated
	 */
	void setLeft(Exp value);

	/**
	 * Returns the value of the '<em><b>Right</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Right</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Right</em>' containment reference.
	 * @see #setRight(Exp)
	 * @see mdsebook.testModels.arith.ArithPackage#getBinExp_Right()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Exp getRight();

	/**
	 * Sets the value of the '{@link mdsebook.testModels.arith.BinExp#getRight <em>Right</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Right</em>' containment reference.
	 * @see #getRight()
	 * @generated
	 */
	void setRight(Exp value);

} // BinExp
