/*
 * generated by Xtext 2.10.0
 */
package mdsebook


/**
 * Initialization support for running Xtext languages without Equinox extension registry.
 */
class FsmStandaloneSetup extends FsmStandaloneSetupGenerated {

	def static void doSetup() {
		new FsmStandaloneSetup().createInjectorAndDoEMFRegistration()
	}
}
