package mdsebook.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import mdsebook.services.FsmGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalFsmParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_STRING", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'machine'", "'['", "'initial'", "']'", "'state'", "'on'", "'input'", "'output'", "'and'", "'go'", "'to'"
    };
    public static final int RULE_STRING=5;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int RULE_ID=4;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int RULE_INT=6;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalFsmParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalFsmParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalFsmParser.tokenNames; }
    public String getGrammarFileName() { return "InternalFsm.g"; }



     	private FsmGrammarAccess grammarAccess;

        public InternalFsmParser(TokenStream input, FsmGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "FiniteStateMachine";
       	}

       	@Override
       	protected FsmGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleFiniteStateMachine"
    // InternalFsm.g:64:1: entryRuleFiniteStateMachine returns [EObject current=null] : iv_ruleFiniteStateMachine= ruleFiniteStateMachine EOF ;
    public final EObject entryRuleFiniteStateMachine() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFiniteStateMachine = null;


        try {
            // InternalFsm.g:64:59: (iv_ruleFiniteStateMachine= ruleFiniteStateMachine EOF )
            // InternalFsm.g:65:2: iv_ruleFiniteStateMachine= ruleFiniteStateMachine EOF
            {
             newCompositeNode(grammarAccess.getFiniteStateMachineRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleFiniteStateMachine=ruleFiniteStateMachine();

            state._fsp--;

             current =iv_ruleFiniteStateMachine; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFiniteStateMachine"


    // $ANTLR start "ruleFiniteStateMachine"
    // InternalFsm.g:71:1: ruleFiniteStateMachine returns [EObject current=null] : (otherlv_0= 'machine' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= '[' otherlv_3= 'initial' ( (otherlv_4= RULE_ID ) ) ( (lv_states_5_0= ruleState ) )* otherlv_6= ']' )? ) ;
    public final EObject ruleFiniteStateMachine() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        EObject lv_states_5_0 = null;



        	enterRule();

        try {
            // InternalFsm.g:77:2: ( (otherlv_0= 'machine' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= '[' otherlv_3= 'initial' ( (otherlv_4= RULE_ID ) ) ( (lv_states_5_0= ruleState ) )* otherlv_6= ']' )? ) )
            // InternalFsm.g:78:2: (otherlv_0= 'machine' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= '[' otherlv_3= 'initial' ( (otherlv_4= RULE_ID ) ) ( (lv_states_5_0= ruleState ) )* otherlv_6= ']' )? )
            {
            // InternalFsm.g:78:2: (otherlv_0= 'machine' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= '[' otherlv_3= 'initial' ( (otherlv_4= RULE_ID ) ) ( (lv_states_5_0= ruleState ) )* otherlv_6= ']' )? )
            // InternalFsm.g:79:3: otherlv_0= 'machine' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= '[' otherlv_3= 'initial' ( (otherlv_4= RULE_ID ) ) ( (lv_states_5_0= ruleState ) )* otherlv_6= ']' )?
            {
            otherlv_0=(Token)match(input,11,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getFiniteStateMachineAccess().getMachineKeyword_0());
            		
            // InternalFsm.g:83:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalFsm.g:84:4: (lv_name_1_0= RULE_ID )
            {
            // InternalFsm.g:84:4: (lv_name_1_0= RULE_ID )
            // InternalFsm.g:85:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_4); 

            					newLeafNode(lv_name_1_0, grammarAccess.getFiniteStateMachineAccess().getNameIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getFiniteStateMachineRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            // InternalFsm.g:101:3: (otherlv_2= '[' otherlv_3= 'initial' ( (otherlv_4= RULE_ID ) ) ( (lv_states_5_0= ruleState ) )* otherlv_6= ']' )?
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==12) ) {
                alt2=1;
            }
            switch (alt2) {
                case 1 :
                    // InternalFsm.g:102:4: otherlv_2= '[' otherlv_3= 'initial' ( (otherlv_4= RULE_ID ) ) ( (lv_states_5_0= ruleState ) )* otherlv_6= ']'
                    {
                    otherlv_2=(Token)match(input,12,FOLLOW_5); 

                    				newLeafNode(otherlv_2, grammarAccess.getFiniteStateMachineAccess().getLeftSquareBracketKeyword_2_0());
                    			
                    otherlv_3=(Token)match(input,13,FOLLOW_3); 

                    				newLeafNode(otherlv_3, grammarAccess.getFiniteStateMachineAccess().getInitialKeyword_2_1());
                    			
                    // InternalFsm.g:110:4: ( (otherlv_4= RULE_ID ) )
                    // InternalFsm.g:111:5: (otherlv_4= RULE_ID )
                    {
                    // InternalFsm.g:111:5: (otherlv_4= RULE_ID )
                    // InternalFsm.g:112:6: otherlv_4= RULE_ID
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getFiniteStateMachineRule());
                    						}
                    					
                    otherlv_4=(Token)match(input,RULE_ID,FOLLOW_6); 

                    						newLeafNode(otherlv_4, grammarAccess.getFiniteStateMachineAccess().getInitialStateCrossReference_2_2_0());
                    					

                    }


                    }

                    // InternalFsm.g:123:4: ( (lv_states_5_0= ruleState ) )*
                    loop1:
                    do {
                        int alt1=2;
                        int LA1_0 = input.LA(1);

                        if ( (LA1_0==15) ) {
                            alt1=1;
                        }


                        switch (alt1) {
                    	case 1 :
                    	    // InternalFsm.g:124:5: (lv_states_5_0= ruleState )
                    	    {
                    	    // InternalFsm.g:124:5: (lv_states_5_0= ruleState )
                    	    // InternalFsm.g:125:6: lv_states_5_0= ruleState
                    	    {

                    	    						newCompositeNode(grammarAccess.getFiniteStateMachineAccess().getStatesStateParserRuleCall_2_3_0());
                    	    					
                    	    pushFollow(FOLLOW_6);
                    	    lv_states_5_0=ruleState();

                    	    state._fsp--;


                    	    						if (current==null) {
                    	    							current = createModelElementForParent(grammarAccess.getFiniteStateMachineRule());
                    	    						}
                    	    						add(
                    	    							current,
                    	    							"states",
                    	    							lv_states_5_0,
                    	    							"mdsebook.Fsm.State");
                    	    						afterParserOrEnumRuleCall();
                    	    					

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop1;
                        }
                    } while (true);

                    otherlv_6=(Token)match(input,14,FOLLOW_2); 

                    				newLeafNode(otherlv_6, grammarAccess.getFiniteStateMachineAccess().getRightSquareBracketKeyword_2_4());
                    			

                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFiniteStateMachine"


    // $ANTLR start "entryRuleState"
    // InternalFsm.g:151:1: entryRuleState returns [EObject current=null] : iv_ruleState= ruleState EOF ;
    public final EObject entryRuleState() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleState = null;


        try {
            // InternalFsm.g:151:46: (iv_ruleState= ruleState EOF )
            // InternalFsm.g:152:2: iv_ruleState= ruleState EOF
            {
             newCompositeNode(grammarAccess.getStateRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleState=ruleState();

            state._fsp--;

             current =iv_ruleState; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleState"


    // $ANTLR start "ruleState"
    // InternalFsm.g:158:1: ruleState returns [EObject current=null] : (otherlv_0= 'state' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= '[' ( (lv_leavingTransitions_3_0= ruleTransition ) )* otherlv_4= ']' )? ) ;
    public final EObject ruleState() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        EObject lv_leavingTransitions_3_0 = null;



        	enterRule();

        try {
            // InternalFsm.g:164:2: ( (otherlv_0= 'state' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= '[' ( (lv_leavingTransitions_3_0= ruleTransition ) )* otherlv_4= ']' )? ) )
            // InternalFsm.g:165:2: (otherlv_0= 'state' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= '[' ( (lv_leavingTransitions_3_0= ruleTransition ) )* otherlv_4= ']' )? )
            {
            // InternalFsm.g:165:2: (otherlv_0= 'state' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= '[' ( (lv_leavingTransitions_3_0= ruleTransition ) )* otherlv_4= ']' )? )
            // InternalFsm.g:166:3: otherlv_0= 'state' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= '[' ( (lv_leavingTransitions_3_0= ruleTransition ) )* otherlv_4= ']' )?
            {
            otherlv_0=(Token)match(input,15,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getStateAccess().getStateKeyword_0());
            		
            // InternalFsm.g:170:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalFsm.g:171:4: (lv_name_1_0= RULE_ID )
            {
            // InternalFsm.g:171:4: (lv_name_1_0= RULE_ID )
            // InternalFsm.g:172:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_4); 

            					newLeafNode(lv_name_1_0, grammarAccess.getStateAccess().getNameIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getStateRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            // InternalFsm.g:188:3: (otherlv_2= '[' ( (lv_leavingTransitions_3_0= ruleTransition ) )* otherlv_4= ']' )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==12) ) {
                alt4=1;
            }
            switch (alt4) {
                case 1 :
                    // InternalFsm.g:189:4: otherlv_2= '[' ( (lv_leavingTransitions_3_0= ruleTransition ) )* otherlv_4= ']'
                    {
                    otherlv_2=(Token)match(input,12,FOLLOW_7); 

                    				newLeafNode(otherlv_2, grammarAccess.getStateAccess().getLeftSquareBracketKeyword_2_0());
                    			
                    // InternalFsm.g:193:4: ( (lv_leavingTransitions_3_0= ruleTransition ) )*
                    loop3:
                    do {
                        int alt3=2;
                        int LA3_0 = input.LA(1);

                        if ( (LA3_0==16) ) {
                            alt3=1;
                        }


                        switch (alt3) {
                    	case 1 :
                    	    // InternalFsm.g:194:5: (lv_leavingTransitions_3_0= ruleTransition )
                    	    {
                    	    // InternalFsm.g:194:5: (lv_leavingTransitions_3_0= ruleTransition )
                    	    // InternalFsm.g:195:6: lv_leavingTransitions_3_0= ruleTransition
                    	    {

                    	    						newCompositeNode(grammarAccess.getStateAccess().getLeavingTransitionsTransitionParserRuleCall_2_1_0());
                    	    					
                    	    pushFollow(FOLLOW_7);
                    	    lv_leavingTransitions_3_0=ruleTransition();

                    	    state._fsp--;


                    	    						if (current==null) {
                    	    							current = createModelElementForParent(grammarAccess.getStateRule());
                    	    						}
                    	    						add(
                    	    							current,
                    	    							"leavingTransitions",
                    	    							lv_leavingTransitions_3_0,
                    	    							"mdsebook.Fsm.Transition");
                    	    						afterParserOrEnumRuleCall();
                    	    					

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop3;
                        }
                    } while (true);

                    otherlv_4=(Token)match(input,14,FOLLOW_2); 

                    				newLeafNode(otherlv_4, grammarAccess.getStateAccess().getRightSquareBracketKeyword_2_2());
                    			

                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleState"


    // $ANTLR start "entryRuleTransition"
    // InternalFsm.g:221:1: entryRuleTransition returns [EObject current=null] : iv_ruleTransition= ruleTransition EOF ;
    public final EObject entryRuleTransition() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTransition = null;


        try {
            // InternalFsm.g:221:51: (iv_ruleTransition= ruleTransition EOF )
            // InternalFsm.g:222:2: iv_ruleTransition= ruleTransition EOF
            {
             newCompositeNode(grammarAccess.getTransitionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleTransition=ruleTransition();

            state._fsp--;

             current =iv_ruleTransition; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTransition"


    // $ANTLR start "ruleTransition"
    // InternalFsm.g:228:1: ruleTransition returns [EObject current=null] : (otherlv_0= 'on' otherlv_1= 'input' ( (lv_input_2_0= RULE_STRING ) ) (otherlv_3= 'output' ( (lv_output_4_0= RULE_STRING ) ) )? otherlv_5= 'and' otherlv_6= 'go' otherlv_7= 'to' ( (otherlv_8= RULE_ID ) ) ) ;
    public final EObject ruleTransition() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token lv_input_2_0=null;
        Token otherlv_3=null;
        Token lv_output_4_0=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_8=null;


        	enterRule();

        try {
            // InternalFsm.g:234:2: ( (otherlv_0= 'on' otherlv_1= 'input' ( (lv_input_2_0= RULE_STRING ) ) (otherlv_3= 'output' ( (lv_output_4_0= RULE_STRING ) ) )? otherlv_5= 'and' otherlv_6= 'go' otherlv_7= 'to' ( (otherlv_8= RULE_ID ) ) ) )
            // InternalFsm.g:235:2: (otherlv_0= 'on' otherlv_1= 'input' ( (lv_input_2_0= RULE_STRING ) ) (otherlv_3= 'output' ( (lv_output_4_0= RULE_STRING ) ) )? otherlv_5= 'and' otherlv_6= 'go' otherlv_7= 'to' ( (otherlv_8= RULE_ID ) ) )
            {
            // InternalFsm.g:235:2: (otherlv_0= 'on' otherlv_1= 'input' ( (lv_input_2_0= RULE_STRING ) ) (otherlv_3= 'output' ( (lv_output_4_0= RULE_STRING ) ) )? otherlv_5= 'and' otherlv_6= 'go' otherlv_7= 'to' ( (otherlv_8= RULE_ID ) ) )
            // InternalFsm.g:236:3: otherlv_0= 'on' otherlv_1= 'input' ( (lv_input_2_0= RULE_STRING ) ) (otherlv_3= 'output' ( (lv_output_4_0= RULE_STRING ) ) )? otherlv_5= 'and' otherlv_6= 'go' otherlv_7= 'to' ( (otherlv_8= RULE_ID ) )
            {
            otherlv_0=(Token)match(input,16,FOLLOW_8); 

            			newLeafNode(otherlv_0, grammarAccess.getTransitionAccess().getOnKeyword_0());
            		
            otherlv_1=(Token)match(input,17,FOLLOW_9); 

            			newLeafNode(otherlv_1, grammarAccess.getTransitionAccess().getInputKeyword_1());
            		
            // InternalFsm.g:244:3: ( (lv_input_2_0= RULE_STRING ) )
            // InternalFsm.g:245:4: (lv_input_2_0= RULE_STRING )
            {
            // InternalFsm.g:245:4: (lv_input_2_0= RULE_STRING )
            // InternalFsm.g:246:5: lv_input_2_0= RULE_STRING
            {
            lv_input_2_0=(Token)match(input,RULE_STRING,FOLLOW_10); 

            					newLeafNode(lv_input_2_0, grammarAccess.getTransitionAccess().getInputSTRINGTerminalRuleCall_2_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getTransitionRule());
            					}
            					setWithLastConsumed(
            						current,
            						"input",
            						lv_input_2_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }

            // InternalFsm.g:262:3: (otherlv_3= 'output' ( (lv_output_4_0= RULE_STRING ) ) )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==18) ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // InternalFsm.g:263:4: otherlv_3= 'output' ( (lv_output_4_0= RULE_STRING ) )
                    {
                    otherlv_3=(Token)match(input,18,FOLLOW_9); 

                    				newLeafNode(otherlv_3, grammarAccess.getTransitionAccess().getOutputKeyword_3_0());
                    			
                    // InternalFsm.g:267:4: ( (lv_output_4_0= RULE_STRING ) )
                    // InternalFsm.g:268:5: (lv_output_4_0= RULE_STRING )
                    {
                    // InternalFsm.g:268:5: (lv_output_4_0= RULE_STRING )
                    // InternalFsm.g:269:6: lv_output_4_0= RULE_STRING
                    {
                    lv_output_4_0=(Token)match(input,RULE_STRING,FOLLOW_11); 

                    						newLeafNode(lv_output_4_0, grammarAccess.getTransitionAccess().getOutputSTRINGTerminalRuleCall_3_1_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getTransitionRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"output",
                    							lv_output_4_0,
                    							"org.eclipse.xtext.common.Terminals.STRING");
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_5=(Token)match(input,19,FOLLOW_12); 

            			newLeafNode(otherlv_5, grammarAccess.getTransitionAccess().getAndKeyword_4());
            		
            otherlv_6=(Token)match(input,20,FOLLOW_13); 

            			newLeafNode(otherlv_6, grammarAccess.getTransitionAccess().getGoKeyword_5());
            		
            otherlv_7=(Token)match(input,21,FOLLOW_3); 

            			newLeafNode(otherlv_7, grammarAccess.getTransitionAccess().getToKeyword_6());
            		
            // InternalFsm.g:298:3: ( (otherlv_8= RULE_ID ) )
            // InternalFsm.g:299:4: (otherlv_8= RULE_ID )
            {
            // InternalFsm.g:299:4: (otherlv_8= RULE_ID )
            // InternalFsm.g:300:5: otherlv_8= RULE_ID
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getTransitionRule());
            					}
            				
            otherlv_8=(Token)match(input,RULE_ID,FOLLOW_2); 

            					newLeafNode(otherlv_8, grammarAccess.getTransitionAccess().getTargetStateCrossReference_7_0());
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTransition"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000001002L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x000000000000C000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000014000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x00000000000C0000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000200000L});

}