package fpinscala.parsing

import scala.language.higherKinds
import scala.language.implicitConversions
import scala.util.matching.Regex


trait Parsers[Parser[+_]] { self =>

  def run[A] (p: Parser[A]) (input: String): Either[ParseError,A]

  def char(c: Char): Parser[Char] = string (c.toString) map (_(0))

  def or[A] (s1: Parser[A], s2: => Parser[A]): Parser[A]

  implicit def string (s: String): Parser[String]
  implicit def operators[A] (p: Parser[A]) = ParserOps[A] (p)
  implicit def asStringParser[A] (a: A) (implicit f: A => Parser[String]) :ParserOps[String] = ParserOps (f(a))


  def succeed[A] (a: A): Parser[A]

  def many1[A] (p: Parser[A]): Parser[List[A]] =  product (p, p.many).map { a => a._1::a._2 }
  // def count[A] (p: Parser[A]): Parser[Int]
  // def countPlus[A] (p: Parser[A]): Parser[Int] // they could also just mean fusing star and plus using laziness

  def map[A,B] (p: Parser[A]) (f: A => B) :Parser[B] =
    flatMap (p) (a => succeed(f(a)))
  def product[A,B] (p1: Parser[A], p2: => Parser[B]) :Parser[(A,B)] =
    flatMap (p1) (a => map(p2)(b => (a,b)))

  def slice[A] (p: Parser[A]) : Parser[String]

  val numA: Parser[Int] = char('a').many.map(_.size)

  def map2[A,B,C] (p: Parser[A], p2: => Parser[B]) (f: (A,B) => C): Parser[C] =
    product (p, p2).map (f.tupled)

  // def many1_ [A] (p: Parser[A]): Parser[List[A]] =
  //   map2[A,List[A],List[A]] (p, p.many) { _ :: _ }

  def many [A] (p: Parser[A]): Parser[List[A]] =
    attempt (map2 (p, many(p)) { _ :: _ }) | succeed (Nil)

  def listOfN[A] (n: Int, p: Parser[A]): Parser[List[A]] = n match {
    case 0 => succeed (Nil)
    case n => map2 (p, listOfN (n-1, p)) { _ :: _ }
  }

  def flatMap[A,B] (p: Parser[A]) (f: A => Parser[B]) : Parser[B]

  implicit def regex (r: Regex): Parser[String]

  // Exercise 9.6 (important)

  val digit :Parser[Int] = regex ("[0-9]".r) map (_.toInt)
  val DEC :Parser[Int] = regex ("""[\+-]?[0-9]+""".r) .map[Int] { _.toInt }

  val ex96 = digit.flatMap { listOfN (_, char('a')) }
  // notice: this is not possible with regular expressions, not even with a
  // context free grammar
  //
  // *** (part of 9.9)

  val spaces :Parser[String] = char(' ').many.slice
  val ws :Parser[Unit] =
    "[\t\n ]+".r
      .map { _ => () } // using many would likely be less efficient


  def opt[A] (p: Parser[A]) :Parser[Option[A]] = p.map { Some(_) } | succeed (None)
  def scope[A] (msg: String) (p: Parser[A]): Parser[A]
  def attempt[A] (p: Parser[A]): Parser[A]

  case class ParserOps[A] (p: Parser[A]) {

    def run (input: String): Either[ParseError,A] = self.run (p) (input)

    def | [B>:A] (p2: => Parser[B]) :Parser[B] = self.or (p,p2)
    def or[B>:A] (p2: => Parser[B]): Parser[B] = self.or (p,p2)

    def ** [B] (that: Parser[B]) :Parser[(A,B)] = self.product (this.p,that)
    def |* [B] (that: Parser[B]) :Parser[B] = self.product (this.p,that) map { _._2 }
    def *| [B] (that: Parser[B]) :Parser[A] = self.product (this.p,that) map { _._1 }
    def product [B] (that: Parser[B]) :Parser[(A,B)] = self.product (this.p,that)
    def ? :Parser[Option[A]] = self.opt (p)
    def * :Parser[List[A]] = self many p

    def many: Parser[List[A]] = self many p
    def slice[A]: Parser[String] = self slice p


    def map[B] (f: A => B) :Parser[B] = self.map (p) (f)
    def flatMap[B] (f: A => Parser[B]) :Parser[B] = self.flatMap (p) (f)

  }

}

case class Location (input: String, offset: Int = 0) {

  lazy val line = input.slice (0, offset+1).count(_ == '\n') + 1
  lazy val col = input.slice(0,offset+1).reverse.indexOf('\n')

  def toError(msg: String): ParseError = ParseError(List((this, msg)))

  def advanceBy(n: Int) = copy(offset = offset+n)

  /* Returns the line corresponding to this location */
  def currentLine: String =
    if (input.length > 1) input.lines.drop(line-1).next
    else ""

  override def toString = s"$line:$col"

  lazy val curr = input.substring (offset)
}


case class ParseError (stack: List[(Location,String)]) {

  def push (loc: Location, msg: String)  :ParseError =
    this.copy (stack = (loc, msg) ::stack)

  def label (msg: String) :ParseError  =
    ParseError (latestLoc.map ((_,msg)).toList)

  def latestLoc: Option[Location] = latest map (_._1)

  def latest: Option[(Location,String)] = stack.lastOption


}


