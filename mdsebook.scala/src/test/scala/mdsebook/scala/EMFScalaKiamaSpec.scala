// (c) mddbook, wasowski, tberger
// Tests for the EMFScala adapation code
// Run using 'sbt test'
package mdsebook.scala

import EMFScala._
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.emf.ecore.util.EcoreUtil
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl
import org.scalatest.FreeSpec
import org.scalatest.Matchers

import mdsebook.testModels.counter.{Counter,CounterPackage}

import org.bitbucket.inkytonik.kiama.rewriting.Rewriter._
import org.bitbucket.inkytonik.kiama.rewriting._

class EMFScalaKiamaSpec extends FreeSpec with Matchers {

  "mdsebook.testModels.counter" - {
    CounterPackage.eINSTANCE.eClass
    val c: Counter = loadFromXMI[Counter] ("../mdsebook.testModels/test-files/counter-00.xmi")

    "increment counter purely, until 100 (endo)" in {

      lazy val inc = rule[Counter] { case c if c.getValue < 100 =>
      EcoreUtil copy c before { c1 => c1 setValue (c1.getValue+1) } }

      c setValue 42
      val c1 :Counter = rewrite (repeat(inc)) (c)

      c1.getValue shouldBe 100
      c.getValue shouldBe 42

      saveAsXMI ("test-gen/counter-00-rewritten.xmi") (c1)

    }
  }

  "mdsebook.testModels.arith" - {

    import mdsebook.testModels.arith._
    ArithPackage.eINSTANCE.eClass
    val factory = ArithFactory.eINSTANCE

    "reduce a value of a simple expression (endo)" in {

      def groundTerm (e :BinExp) : Boolean =
        e.getLeft.isInstanceOf[ConstInt] && e.getRight.isInstanceOf[ConstInt]

      val eval = rule[Exp] {

        case e :PlusExp if groundTerm (e) =>
              factory.createConstInt before
              { _ setValue (e.getLeft.asInstanceOf[ConstInt].getValue
                            +
                            e.getRight.asInstanceOf[ConstInt].getValue) }

        case e :SubExp if groundTerm (e) =>
              factory.createConstInt before
              { _ setValue (e.getLeft.asInstanceOf[ConstInt].getValue
                            -
                            e.getRight.asInstanceOf[ConstInt].getValue) }
      }

      def apply (s: => Strategy): Strategy = rulefs[BinExp] { case _ :BinExp => congruence (s,s) }

      val e: Exp = loadFromXMI[Exp] ("../mdsebook.testModels/test-files/arith-00.xmi")
      val eo :Exp = rewrite (reduce (eval)) (e)

      val l: SubExp = e.asInstanceOf[BinExp].getLeft.asInstanceOf[SubExp]
      val lo :Exp = rewrite (topdown (apply (eval))) (l)

      val r: PlusExp = e.asInstanceOf[BinExp].getRight.asInstanceOf[PlusExp]
      val ro :Exp = rewrite (topdown (apply (eval))) (r)

      val rr :PlusExp = r.getRight.asInstanceOf[PlusExp]

      eo should not be e
      eo shouldBe a [ConstInt]
      saveAsXMI ("test-gen/arith-00-rewritten.xmi") (eo)
    }


  }





}
