/**
 */
package mdsebook.pascal;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Entry</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link mdsebook.pascal.Entry#getNext <em>Next</em>}</li>
 *   <li>{@link mdsebook.pascal.Entry#getRow <em>Row</em>}</li>
 *   <li>{@link mdsebook.pascal.Entry#getValue <em>Value</em>}</li>
 *   <li>{@link mdsebook.pascal.Entry#getLeftParent <em>Left Parent</em>}</li>
 *   <li>{@link mdsebook.pascal.Entry#getRightParent <em>Right Parent</em>}</li>
 * </ul>
 *
 * @see mdsebook.pascal.PascalPackage#getEntry()
 * @model
 * @generated
 */
public interface Entry extends EObject {
	/**
	 * Returns the value of the '<em><b>Next</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Next</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Next</em>' reference.
	 * @see #setNext(Entry)
	 * @see mdsebook.pascal.PascalPackage#getEntry_Next()
	 * @model
	 * @generated
	 */
	Entry getNext();

	/**
	 * Sets the value of the '{@link mdsebook.pascal.Entry#getNext <em>Next</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Next</em>' reference.
	 * @see #getNext()
	 * @generated
	 */
	void setNext(Entry value);

	/**
	 * Returns the value of the '<em><b>Row</b></em>' attribute.
	 * The default value is <code>"1"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Row</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Row</em>' attribute.
	 * @see #setRow(int)
	 * @see mdsebook.pascal.PascalPackage#getEntry_Row()
	 * @model default="1" required="true"
	 * @generated
	 */
	int getRow();

	/**
	 * Sets the value of the '{@link mdsebook.pascal.Entry#getRow <em>Row</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Row</em>' attribute.
	 * @see #getRow()
	 * @generated
	 */
	void setRow(int value);

	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * The default value is <code>"1"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see #setValue(int)
	 * @see mdsebook.pascal.PascalPackage#getEntry_Value()
	 * @model default="1" required="true"
	 * @generated
	 */
	int getValue();

	/**
	 * Sets the value of the '{@link mdsebook.pascal.Entry#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(int value);

	/**
	 * Returns the value of the '<em><b>Left Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Left Parent</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Left Parent</em>' reference.
	 * @see #setLeftParent(Entry)
	 * @see mdsebook.pascal.PascalPackage#getEntry_LeftParent()
	 * @model
	 * @generated
	 */
	Entry getLeftParent();

	/**
	 * Sets the value of the '{@link mdsebook.pascal.Entry#getLeftParent <em>Left Parent</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Left Parent</em>' reference.
	 * @see #getLeftParent()
	 * @generated
	 */
	void setLeftParent(Entry value);

	/**
	 * Returns the value of the '<em><b>Right Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Right Parent</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Right Parent</em>' reference.
	 * @see #setRightParent(Entry)
	 * @see mdsebook.pascal.PascalPackage#getEntry_RightParent()
	 * @model
	 * @generated
	 */
	Entry getRightParent();

	/**
	 * Sets the value of the '{@link mdsebook.pascal.Entry#getRightParent <em>Right Parent</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Right Parent</em>' reference.
	 * @see #getRightParent()
	 * @generated
	 */
	void setRightParent(Entry value);

} // Entry
