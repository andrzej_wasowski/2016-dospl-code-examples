/**
 */
package mdsebook.pascal;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Triangle</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link mdsebook.pascal.Triangle#getCoefficients <em>Coefficients</em>}</li>
 *   <li>{@link mdsebook.pascal.Triangle#getRoot <em>Root</em>}</li>
 * </ul>
 *
 * @see mdsebook.pascal.PascalPackage#getTriangle()
 * @model
 * @generated
 */
public interface Triangle extends EObject {
	/**
	 * Returns the value of the '<em><b>Coefficients</b></em>' containment reference list.
	 * The list contents are of type {@link mdsebook.pascal.Entry}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Coefficients</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Coefficients</em>' containment reference list.
	 * @see mdsebook.pascal.PascalPackage#getTriangle_Coefficients()
	 * @model containment="true"
	 * @generated
	 */
	EList<Entry> getCoefficients();

	/**
	 * Returns the value of the '<em><b>Root</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Root</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Root</em>' reference.
	 * @see #setRoot(Entry)
	 * @see mdsebook.pascal.PascalPackage#getTriangle_Root()
	 * @model required="true"
	 * @generated
	 */
	Entry getRoot();

	/**
	 * Sets the value of the '{@link mdsebook.pascal.Triangle#getRoot <em>Root</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Root</em>' reference.
	 * @see #getRoot()
	 * @generated
	 */
	void setRoot(Entry value);

} // Triangle
