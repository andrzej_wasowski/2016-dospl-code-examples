// (c) mdsebook, wasowski, tberger

package mdsebook.featuremodels1.scala

import scala.collection.JavaConversions._ // for natural access to EList
import mdsebook.scala.EMFScala._
import mdsebook.featuremodels1._

object Constraints {

  val invariants: List[Constraint] = List (

    inv[Model1] { m => true }

  )

}
