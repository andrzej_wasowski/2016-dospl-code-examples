/**
 */
package mdsebook.microEcore;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>EStructural Feature</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see mdsebook.microEcore.MicroEcorePackage#getEStructuralFeature()
 * @model abstract="true"
 * @generated
 */
public interface EStructuralFeature extends ETypedElement {
} // EStructuralFeature
