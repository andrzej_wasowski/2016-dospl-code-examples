/**
 */
package mdsebook.microEcore;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>EPackage</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link mdsebook.microEcore.EPackage#getEClassifiers <em>EClassifiers</em>}</li>
 *   <li>{@link mdsebook.microEcore.EPackage#getESuperPackage <em>ESuper Package</em>}</li>
 *   <li>{@link mdsebook.microEcore.EPackage#getESubpackages <em>ESubpackages</em>}</li>
 * </ul>
 *
 * @see mdsebook.microEcore.MicroEcorePackage#getEPackage()
 * @model
 * @generated
 */
public interface EPackage extends ENamedElement {
	/**
	 * Returns the value of the '<em><b>EClassifiers</b></em>' containment reference list.
	 * The list contents are of type {@link mdsebook.microEcore.EClassifier}.
	 * It is bidirectional and its opposite is '{@link mdsebook.microEcore.EClassifier#getEPackage <em>EPackage</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>EClassifiers</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>EClassifiers</em>' containment reference list.
	 * @see mdsebook.microEcore.MicroEcorePackage#getEPackage_EClassifiers()
	 * @see mdsebook.microEcore.EClassifier#getEPackage
	 * @model opposite="ePackage" containment="true"
	 * @generated
	 */
	EList<EClassifier> getEClassifiers();

	/**
	 * Returns the value of the '<em><b>ESuper Package</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link mdsebook.microEcore.EPackage#getESubpackages <em>ESubpackages</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>ESuper Package</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ESuper Package</em>' container reference.
	 * @see #setESuperPackage(EPackage)
	 * @see mdsebook.microEcore.MicroEcorePackage#getEPackage_ESuperPackage()
	 * @see mdsebook.microEcore.EPackage#getESubpackages
	 * @model opposite="ESubpackages" transient="false"
	 * @generated
	 */
	EPackage getESuperPackage();

	/**
	 * Sets the value of the '{@link mdsebook.microEcore.EPackage#getESuperPackage <em>ESuper Package</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ESuper Package</em>' container reference.
	 * @see #getESuperPackage()
	 * @generated
	 */
	void setESuperPackage(EPackage value);

	/**
	 * Returns the value of the '<em><b>ESubpackages</b></em>' containment reference list.
	 * The list contents are of type {@link mdsebook.microEcore.EPackage}.
	 * It is bidirectional and its opposite is '{@link mdsebook.microEcore.EPackage#getESuperPackage <em>ESuper Package</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>ESubpackages</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ESubpackages</em>' containment reference list.
	 * @see mdsebook.microEcore.MicroEcorePackage#getEPackage_ESubpackages()
	 * @see mdsebook.microEcore.EPackage#getESuperPackage
	 * @model opposite="eSuperPackage" containment="true"
	 * @generated
	 */
	EList<EPackage> getESubpackages();

} // EPackage
