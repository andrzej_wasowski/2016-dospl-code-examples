/**
 */
package mdsebook.microEcore;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>EAttribute</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link mdsebook.microEcore.EAttribute#getEAttributeType <em>EAttribute Type</em>}</li>
 * </ul>
 *
 * @see mdsebook.microEcore.MicroEcorePackage#getEAttribute()
 * @model
 * @generated
 */
public interface EAttribute extends EStructuralFeature {
	/**
	 * Returns the value of the '<em><b>EAttribute Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>EAttribute Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>EAttribute Type</em>' reference.
	 * @see mdsebook.microEcore.MicroEcorePackage#getEAttribute_EAttributeType()
	 * @model required="true" transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	EDataType getEAttributeType();

} // EAttribute
