// (c) mdsebook, wasowski, tberger

package mdsebook.relationalmodelfk.scala

import scala.collection.JavaConversions._ // for natural access to EList
import mdsebook.scala.EMFScala._
import mdsebook.relationalmodelfk._

object Constraints {

  val invariants: List[Constraint] = List (

    inv[Root] { _ => true } // modify this constraint

  )

}
