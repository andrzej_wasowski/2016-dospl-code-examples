/**
 */
package mdsebook.pipes;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Internal Node</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see mdsebook.pipes.PipesPackage#getInternalNode()
 * @model
 * @generated
 */
public interface InternalNode extends Sink, Source {
} // InternalNode
